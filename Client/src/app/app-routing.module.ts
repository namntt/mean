import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { ProduitsComponent } from './produits/produits.component';
import { CategoriesComponent } from './categories/categories.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { PanierComponent } from './panier/panier.component';


const routes: Routes = [
  {
    path: 'utilisateurs/connexion',
    component: ConnexionComponent
  }, 
  {
    path: 'categories',
    component: CategoriesComponent
  }, 
  {
    path: 'produits/:categorie',
    component: ProduitsComponent
  }, 
  {
    path: 'utilisateurs/inscription',
    component: InscriptionComponent
  },
  {
    path: 'panier',
    component: PanierComponent
  },
  {
    path: 'produits',
    component: ProduitsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
