import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject, Observable } from 'rxjs';


const httpOption = {
  headers: new HttpHeaders({
    "Access-Control-Allow-Methods": "GET, POST", 
    "Access-Control-Allow-Headers": "Content-Type", 
    "Access-Control-Allow-Origin": "*", 
    "Content-Type": "application/json",
  })
};

@Injectable({
  providedIn: 'root'
})

export class AuthentificationService {
  private user:Subject<string> = new BehaviorSubject<string>(undefined);
  private baseURL: string = "http://localhost:8888/";

  constructor(private http: HttpClient) { }

  getUser() { return this.user; }

  connect(data: string) { this.user.next(data); }
  disconnect() { this.user.next(null); }

  verificationConnexion(iden): Observable<any> {
    return this.http.post(this.baseURL + 'utilisateurs/connexion', JSON.stringify(iden), httpOption);
  }
}
