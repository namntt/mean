import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthentificationService } from '../authentification.service';
import { ProduitsService } from '../produits.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  private user: Observable<string>;
  private categories: String[] = new Array();

  constructor(private router: Router, private authService: AuthentificationService, private proService: ProduitsService) {
    this.user = this.authService.getUser();
   }

  ngOnInit() {
    this.proService.getCategorie().subscribe(categories => {
      this.categories = categories;
    });
  }

  getProduitsParCategories(categorie){
    this.router.navigate(['/produits', categorie]);
  }

}
