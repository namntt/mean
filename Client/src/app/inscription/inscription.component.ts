import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent {
  email: string;
  nom: string;
  pnom: string;
  pw: string;
  gdr: string;

  private utlst: Object;
  public message: string = "";

  constructor(private httpClient: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.ajouterUtlst(
      this.email, 
      this.pw, 
      this.nom, 
      this.pnom, 
      this.gdr
    ).subscribe(response => {
      this.message = response['message'];
      this.utlst = response;
      if (response['resultat']){
        this.router.navigate(['/utilisateurs/connexion']);
      }      
    });
  }

  ajouterUtlst(email, pw, nom, pnom, gdr): Observable<any> {
    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post("http://localhost:8888/utilisateurs/inscription", 
      {
        "email": email,
        "password": pw, 
        "nom": nom,
        "prenom": pnom,
        "gendre": gdr
      }, httpOption);
  }

}
