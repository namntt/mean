import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProduitsService {
  private urlBase: string = 'http://localhost:8888/';
  
  constructor(private http: HttpClient) { }

  getProduits() : Observable<any>{
    return this.http.get(this.urlBase + 'produits');
  }

  getProduitParCategorie(categorie) : Observable<any>{
    return this.http.get(this.urlBase + 'produits/' + categorie);
  }

  getCategorie() : Observable<any>{
    return this.http.get(this.urlBase + 'categories');
  }

  createProduit(){
    
  }

  deleteProduit(){

  }

  searchProduit(){

  }

}
