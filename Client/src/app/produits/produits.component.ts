import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { ProduitsService } from '../produits.service';
import { AuthentificationService } from '../authentification.service';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})
export class ProduitsComponent implements OnInit {

  private user: Observable<string>;
  private produits: Object[] = new Array();

  constructor(private route: ActivatedRoute, private produitService: ProduitsService, private authService: AuthentificationService, ) {
    this.user = this.authService.getUser();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      console.log("Dans produits.component.ts avec " + params["categorie"]);
      if (params["categorie"] !== undefined) {
        console.log("/produits/" + params['categorie']);
        this.produitService.getProduitParCategorie(params["categorie"]).subscribe(produits => {
          this.produits = produits;
        });
      } else {
        this.produitService.getProduits().subscribe(produits => {
          this.produits = produits;
        });
      }
    });
  }

}
