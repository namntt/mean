const express = require("express");
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "*");
  next();
});
//app.use(require("cors"));

const MongoClient = require("mongodb").MongoClient;
const ObjectID = require("mongodb").ObjectId;
const url = "mongodb://localhost:27017";

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
  let db = client.db("SUPERVENTES");

  /* LISTE DE PRODUIT */
  app.get("/produits", (req, res) => {
    console.log("/produits");
    try {
      db.collection("produits")
        .find()
        .toArray((err, documents) => {
          res.end(JSON.stringify(documents));
        });
    } catch (e) {
      console.log("Erreur sur /produits : " + e);
      res.end(JSON.stringify([]));
    }
  });

  //ajout de produit   
  app.post("/produits/ajouter", (req, res) => {
    console.log("Ajouté sous " + JSON.stringify(req.body));
    try {
      let nom = req.body.nom;
      let type = req.body.type;
      let marque = req.body.marque;
      let prix = req.body.prix;
      let qte = req.body.qte;

      let data = {
        nom: nom,
        type: type,
        marque: marque,
        prix: prix,
        qte: qte
      };
      prodCollection = db.collection("produits");
      prodCollection.findOne({ nom: req.body.nom, marque: req.body.marque }, (err, documents) => {
        if (err) console.log(err);
        if (documents !== null) {
          prodCollection.updateOne({ nom: req.body.nom }, { $inc: { qte: 1 } }); 
          res.end(
            JSON.stringify({
              resultat: 0,
              message: "Produit existé, quantité augmenté de 1"
            })
          );
        } else {
          db.collection("produits").insertOne(data, (err, documents) => {
            if (err) throw err;
            res.end(
              JSON.stringify({
                resultat: 1,
                message: "Produit ajouté"
              })
            );
          });
        }
      });
    } catch (e) {
      res.end(
        JSON.stringify({
          resultat: 0,
          message: "Erreur sur l'ajoute de produit: " + e
        })
      );
    }
  });

  //categorie
  app.get("/produits/:categorie", (req, res) => {
    let categorie = req.params.categorie;
    console.log("/produits/" + categorie);
    try {
      db.collection("produits")
        .find({ type: categorie })
        .toArray((err, documents) => {
          res.end(JSON.stringify(documents));
        });
    } catch (e) {
      console.log("Erreur sur /produits/" + categorie + " : " + e);
      res.end(JSON.stringify([]));
    }
  });

  //liste de categories
  app.get("/categories", (req, res) => {
    console.log("/categories");
    categories = [];
    try {
      db.collection("produits")
        .find()
        .toArray((err, documents) => {
          for (let doc of documents) {
            if (!categories.includes(doc.type)) {
              categories.push(doc.type);
            }
          }
          console.log("Renvoi de " + JSON.stringify(categories));
          res.end(JSON.stringify(categories));
        });
    } catch (e) {
      console.log("Erreur sur /categories : " + e);
      res.end(JSON.stringify([]));
    }
  });

  //connexion
  app.post("/utilisateurs/connexion", (req, res) => {
    console.log("/utilisateurs/connexion avec " + JSON.stringify(req.body));
    try {
      db.collection("utilisateurs")
        .find(req.body)
        .toArray((err, documents) => {
          if (documents == {}) {
            console.log("null response");
          }
          if (documents.length == 1) {
            res.end(
              JSON.stringify({
                resultat: 1,
                message: "Authentification réussie"
              })
            );
          } else {
            res.end(
              JSON.stringify({
                resultat: 0,
                message: "Email et/ou mot de passe incorrect"
              })
            );
          }
        });
    } catch (e) {
      res.end(
        JSON.stringify({
          resultat: 0,
          message: e
        })
      );
    }
  });

  //list d'utilisateur
  app.get("/utilisateurs", (req, res) => {
    console.log("/utilisateurs");
    try {
      db.collection("utilisateurs")
        .find()
        .toArray((err, documents) => {
          res.end(JSON.stringify(documents));
        });
    } catch (e) {
      console.log("Erreur sur /utilisateurs : " + e);
      res.end(JSON.stringify([]));
    }
  });

  //ajout d'utilisateurs (inscription)
  app.post("/utilisateurs/inscription", (req, res) => {
    console.log("S'inscrire sous " + JSON.stringify(req.body));
    try {
      let nom = req.body.nom;
      let prenom = req.body.prenom;
      let password = req.body.password;
      let gendre = req.body.gendre;
      let email = req.body.email;

      let data = {
        nom: nom,
        prenom: prenom,
        password: password,
        gendre: gendre,
        email: email
      };

      db.collection("utilisateurs").findOne(
        { email: req.body.email },
        (err, documents) => {
          if (err) console.log(err);
          if (documents !== null) {
            res.end(
              JSON.stringify({
                resultat: 0,
                message: "Email existé"
              })
            );
          } else {
            db.collection("utilisateurs").insertOne(data, (err, documents) => {
              if (err) throw err;
              res.end(
                JSON.stringify({
                  resultat: 1,
                  message: "Inscription réussie"
                })
              );
            });
          }
        }
      );
    } catch (e) {
      res.end(
        JSON.stringify({
          resultat: 0,
          message: "Erreur sur l'inscription: " + e
        })
      );
    }
  });

  //ajout de produits au panier
  app.post("/panier", (req, res) => {
    console.log("ajout au panier le produit: " + JSON.stringify(req.body));
    try {
      let articles = [];
      let nom = null;
      let totals = 0;
      let isIn = false;

      let data = {
        articles: articles,
        totals: totals,
        isIn: isIn
      };

      let panier = JSON.parse(req.body.panier);
      if (!panier) {
        return res.json(articles);
      }

      db.collection('produit').find().toArray((err, documents) => {
        for (let i = 0; i < documents.length; i++) {
          nom = documents[i].nom.toString();
          if (panier.hasOwnProperty(nom)) {
            
          }
        }
      });

      
    } catch (e) {
      res.end(JSON.stringify({
          resultat: 0,
          message: "Erreur sur le panier: " + e
      }));      
    }
  });

  //rechercher multi-critere - /produits/recherche/(param nom)/(param prixmin)/(param prixmax)/.../...
  app.get("/produits/recherche/:nom/:marque/:prixmin", (req, res) => {
      try {
        let nom = req.params.nom || '';
        let marque = req.params.marque || '';
        let prixmax = req.params.prixmax || '' ;
        console.log(JSON.stringify(req.body));
        let data = {
          nom: nom,
          marque: marque, 
          prix: { $lt: prixmax }
        };
        db.collection("produits").find(data).toArray((err, documents) =>{
          res.end(JSON.stringify(documents));
        });        
      } catch (e) {
        console.log("Erreur : " + e);
        res.end(JSON.stringify([]));
      }
    }
  );
});

app.listen(8888);
